﻿using notes2.Classes;
using notes2.Migrations;
using notes2.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace notes2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<NotesContext, Configuration>());
            AreaRegistration.RegisterAllAreas();
            this.CheckRoles();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void CheckRoles()
        {
            Utilities.CheckRole(Utilities.Admin);
            Utilities.CheckRole(Utilities.Student);
            Utilities.CheckRole(Utilities.Teacher);
        }
    }
}
