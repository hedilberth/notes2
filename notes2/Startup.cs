﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(notes2.Startup))]
namespace notes2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
