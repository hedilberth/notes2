﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace notes2.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        [Required]
        [Display(Name ="E-Mail")]
        [DataType(DataType.EmailAddress)]
        [StringLength(100,ErrorMessage ="The field {0} maximun lenth is {1} and the minimum is {2}")]
        [Index("UserNameIndex",IsUnique =true)]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "Firts name")]
        [StringLength(50, ErrorMessage = "The field {0} maximun lenth is {1} and the minimum is {2}")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last name")]
        [StringLength(50, ErrorMessage = "The field {0} maximun lenth is {1} and the minimum is {2}")]
        public string LastName { get; set; }

        [Display(Name ="User")]
        public string Fullname { get { return string.Format("{0} {1}", this.FirstName,this.LastName); } }

        [Required]
        [StringLength(20,ErrorMessage ="The field {0} maximun length is {1} and the minimun {2}")]
        public string Phone { get; set; }

        [Required]
        [StringLength(100,ErrorMessage ="The field {0} maximun lenght us {1} and the minimun is {2}")]
        public string Adress { get; set; }

        [DataType(DataType.ImageUrl)]
        public string Photo { get; set; }

        [Display(Name ="Is Student")]
        public bool IsStudent { get; set; }
        [Display(Name = "Is Teacher")]
        public bool IsTeacher { get; set; }
    }
}