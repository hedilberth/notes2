﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace notes2.Models
{
    public class NotesContext : DbContext
    {
        public NotesContext() : base("DefaultConnection")
        {
        }
        protected override void  Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        public System.Data.Entity.DbSet<notes2.Models.User> Users { get; set; }
    }
}